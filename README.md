# Pylic

[Documentation](https://da_doomer.gitlab.io/pylic) | [Examples](/examples)

**Use source code to solve control problems.**

This library helps you formulate and decompose control problems by exploiting source code. The core idea is to describe the control problem as a property (or properties) of the execution of the simulation of the system you want to control. These properties are described to the system as predicates over the execution of the source code. In this view, solving a control problem corresponds to finding an input to the simulator which induces an execution trace that satisfies your predicate.

```sh
pip install git+https://gitlab.com/da_doomer/pylic.git@0.1.1
```


## Examples

### Push button and go to the goal

![Task: solve predicate that encodes "push button AND go to the goal"](examples/ant/main.webm)

Code: [examples/ant/main.py](examples/ant/main.py)

### Thrusted marble maze planning

![Thrusted marble control signal found by pylic](examples/planning_marble/main_task9.webm)

Code: [examples/planning_marble/main.py](examples/planning_marble/main.py)

### Button password planning

![Password "2,0" found through symbolic planning. The center of the body has to be above a button to activate it.](examples/planning_buttons/main.webm)

Code: [examples/ant/main.py](examples/planning_buttons/main.py)

### Colliding with all boxes

![Platform placement found through symbolic planning. The task is to touch all platforms](examples/marble_drop/trajectory_pylic.mp4)

Code [examples/marble_drop/trajectoy_pylic.py](examples/marble_drop/trajectory_pylic.py)
