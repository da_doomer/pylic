"""
# What is Pylic?

Pylic is a library that helps you formulate and decompose control problems by exploiting source code. Generally speaking, to use Pylic you need:

- A Python simulation of the system you want to control.
- A way to describe your control problem as a property of the execution of the simulator.

# Quickstart

Take a look at the [marble example](https://gitlab.com/da_doomer/pylic/-/tree/master/examples/marble/main.py) to see how Pylic is used to solve a navigation problem.

You can follow the steps below to run the code:

```bash
# Install a stable version of Pylic in an environment with Python `>=3.10`
pip install git+https://gitlab.com/da_doomer/pylic.git@0.1.1

# Download example from git repo
# (this line will download the folder from gitlab)
wget "https://gitlab.com/da_doomer/pylic/-/archive/master/pylic-master.zip?path=examples/marble"; unzip pylic-master.zip\?path=examples\%2Fmarble; cd pylic-master-examples-marble/examples/marble;

# Execute example
python main.py
```

Keep in mind Pylic is evolving and the API is subject to change.

# What are the main concepts in Pylic?

## Predicates

pylic.predicates

The core idea is to describe the control problem as a property (or properties) of the execution of the simulation of the system you would like to control. You describe these properties as predicates over the execution of the simulator source code. In this view, solving a control problem corresponds to finding an input to the simulator which induces an execution trace that satisfies your predicate.

## Tapes

pylic.tape

A tape is a list of the program states that were encountered while executing a piece of Python code. In principle you can trace any Python code, even if its not a simulator.

## Solvers

pylic.solvers

The process of finding an input that satisfies a given predicate is called "solving." In Pylic, solving a predicate is framed as an optimization process, and so, solving a given predicate is the job of a numerical search algorithm (e.g., gradient descent).

Pylic includes compatibility layers for a few different numerical search libraries, and integrating other libraries should be straightforward.

## Planning

pylic.planner

Often, control problems result in non-convex optimization problems that are very difficult to solve in practice. Pylic naturally lends itself to tackling problems that can be decomposed into discrete parts, defining a predicate for each part and them incrementally. This is analogous to many traditional planning approaches.

## Code transformations

pylic.code_transformations

Pylic transforms a subset of Python code so that predicates can be solved by numerical optimization algorithms. This happens automatically, behind the scenes. Keep in mind that you can optionally assign custom labels to control-flow statements, should you wish to refer to them in your predicates (see pylic.code_transformations.node_id_regex).

The following caveats are present in the Pylic version:

- Program state is not deep-copied during program tracing. The presence of side-effects has wide implications, and not deep-copying state is a deliberate decision to simplify program tracing. In particular, this means that modifying objects may also mutate snapshots of the program state, so mutating should be avoided to avoid subtle implementation errors.
- Not all Python boolean conditions are transformed into differentiable statements.
"""
