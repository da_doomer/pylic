"""Tasks abstract the concept of "finding the parameters of a system
such that its 'Marble' reaches a target position".
"""
from dataclasses import dataclass
from dynamics import Circle, State, Segment
import torch


@dataclass(frozen=True, eq=True)
class Goal:
    position: torch.Tensor
    radius: float


@dataclass(frozen=True, eq=True)
class TaskImpulse:
    """A Task is defined by an initial marble position and velocity, a target
    circle, a list of segment segments and a number of time-steps.

    The task is to find an angle (parametrized in [-1, 1]) such that the
    marble reaches the target circle within the specified number of
    time-steps.

    It is assumed that `timesteps` is greater than 1.
    """
    goal_circle: Goal
    initial_state: State
    max_timesteps: int

example_task = TaskImpulse(
    goal_circle=Goal(torch.tensor([-1.46, 7.5]), 0.2),
    initial_state=State(
        coefficient_of_restitution=0.9,
        drag_constant=0.9,
        dt=0.1,
        impulse_scale=0.8,
        marble=Circle(
            velocity=torch.tensor([0.0, 0.0]),
            position=torch.tensor([0.0, 0.0]),
            radius=0.05,
        ),
        segments=(
            Segment(
                p1=(-0.33, 3.12),
                p2=(0.33, 3.12),
                radius=0.0099,
            ),
            Segment(
                p1=(-2.57, 1.56),
                p2=(-2.61, 2.49),
                radius=0.0099,
            ),
            Segment(
                p1=(-0.7, 3.88),
                p2=(-0.76, 3.62),
                radius=0.0099,
            ),
            Segment(
                p1=(-0.7, 3.88),
                p2=(-0.75, 3.62),
                radius=0.0099,
            ),
            Segment(
                p1=(-0.59, 5.24),
                p2=(-0.62, 4.6),
                radius=0.009,
            )
        ),
    ),
    max_timesteps=240,
)

