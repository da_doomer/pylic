import enum
from dataclasses import dataclass
import random


class Direction(enum.Enum):
    UP = enum.auto()
    DOWN = enum.auto()
    LEFT = enum.auto()
    RIGHT = enum.auto()


@dataclass(eq=True, frozen=True)
class Maze:
    """Sequence of corridor directions that do not intersect
    each other. Each corridor direction is defined as taking
    one unit of distance."""
    directions: tuple[Direction, ...]


def get_serpent(
        maze_len: int,
        serpent_width: int,
        serpent_height: int,
        ) -> Maze:
    serpent = list[Direction]()
    while len(serpent) < maze_len:
        for _ in range(serpent_height):
            serpent.append(Direction.UP)
        for _ in range(serpent_width):
            serpent.append(Direction.RIGHT)
        for _ in range(serpent_height):
            serpent.append(Direction.DOWN)
        for _ in range(serpent_width):
            serpent.append(Direction.RIGHT)
    serpent = serpent[:maze_len]
    return Maze(directions=tuple(serpent))


def get_random_maze(
        maze_len: int,
        seed: str,
        ) -> Maze:
    _random = random.Random(seed)
    directions = list(Direction)
    while True:
        maze = list[Direction]()
        i = 0
        j = 0
        used_coordinates = [(i, j)]
        for _ in range(maze_len+1):
            if len(maze) > 0:
                available_directions = [
                    d for d in directions if d != maze[-1]
                ]
            else:
                available_directions = list(directions)
            _random.shuffle(available_directions)
            ni, nj = None, None
            direction = None
            for cdirection in available_directions:
                ni, nj = i, j
                if cdirection is Direction.UP:
                    nj += 1
                elif cdirection is Direction.DOWN:
                    nj -= 1
                elif cdirection is Direction.LEFT:
                    ni -= 1
                elif cdirection is Direction.RIGHT:
                    ni += 1
                if (ni, nj) not in used_coordinates:
                    direction = cdirection
                    break
                ni, nj = None, None
            if direction is None:
                break
            i, j = ni, nj
            used_coordinates.append((i, j))
            maze.append(direction)
        return Maze(directions=tuple(maze))
