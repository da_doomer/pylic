"""Plotting utilities for the ant environment."""
from environment import get_simulation
from dynamics import simulation
import tempfile
from pathlib import Path
import PIL.Image
import ffmpeg
import torch


def plot_animation(parameters: torch.Tensor, fps: int, output_path: Path):
    """Run an ant simulation using the given parameters as control signal. The
    `parameters` define the time-varying control signal at each step and is
    expected to be of shape `(num_timesteps, 8)`."""
    # Initialize the environment
    env = get_simulation(animation_fps=fps)
    simulation(parameters, env=env)

    # Save each frame
    with tempfile.TemporaryDirectory() as tdir:
        frame_dir = Path(tdir)
        # Plot final state for an additional second
        for i, frame in enumerate(env.video+[env.video[-1] for _ in range(fps)]):
            path = frame_dir/(f"{i}.png").rjust(10)
            img = PIL.Image.fromarray(frame)
            img.save(path)
        (
            ffmpeg
            .input(frame_dir/"*.png", pattern_type="glob", framerate=fps)
            .output(str(output_path))
            .overwrite_output()
            .run(quiet=True)
        )
