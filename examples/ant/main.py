"""A simple example of using pylic for exploiting program structure in a
locomotion task.

The task is to reach a target location, which requires pushing a button
to remove obstacles. This structure is reflected in the simulator. We use
the simulator code to instruct pylic how to optimize parameters.

The solution is found by iteratively solving the conjunction of "push button
AND reach the goal".
"""
from pathlib import Path
from pylic.solvers.cma_es import solver
from pylic.predicates import Conjunction
from pylic.predicates import FunctionCall
from pylic.predicates import IfOr
from pylic.predicates import LessThan
from pylic.predicates import Filter
from pylic.predicates import Constants
from dynamics import simulation
from plotting import plot_animation
import torch
import cma

output_path = Path()/"ant_test"
output_path.mkdir()


# Define the predicate that we want to solve In this example we showcase
# defining custom functions and filters inside the predicates. See the marble
# example to see how a dictionary can be used instead.
action_n = 6
predicate = Conjunction([
    ## (push_button(t=0) OR push_button(t=1) OR ... (t<=half_the_simulation)
    IfOr(
        trace=Filter(
            custom_filter=lambda trace, i:\
                trace[i].id == 'button_check'\
                and trace[i].program_state['t']<action_n//2,
            trace=Constants.input_trace,
        )
    ),

    ## (dist_to_goal[end_of_sim] < 0.1)
    LessThan(
        left=FunctionCall(
            custom_function=lambda trace:\
                (
                    torch.tensor([
                        trace[-1].program_state['env'].goal.x,
                        trace[-1].program_state['env'].goal.y,
                    ])-torch.tensor(
                        trace[-1].program_state['ant_position']
                    )
                ).norm(),
            trace=Constants.input_trace,
        ),
        right=torch.tensor(0.01),
    ),
])

# Iteratively solve the predicate with pylic's gradient descent solver
parameters = torch.zeros((action_n, 8))
for i in range(len(predicate.operands)):
    # Form a sub-predicate with the first i clauses
    sub_predicate = Conjunction(predicate.operands[:i+1])

    # Freeze the found parameters so far and optimize a new postfix
    sub_parameters = torch.zeros((action_n//len(predicate.operands), 8))
    def candidate_processor(p: torch.Tensor) -> torch.Tensor:
        if i == 0:
            return p
        return torch.cat([parameters, p])

    # Call the solver
    p, _ = solver(
        sub_predicate,
        starting_parameters=sub_parameters,
        f=simulation,
        max_value=torch.tensor(10),
        max_f_eval_n=2000000,
        initial_stdev=0.1,
        verbose=True,
        multiprocessing_workers=12,
        opts=cma.evolution_strategy.CMAOptions(
            popsize=12*2,
            seed=289518,
        ),
        candidate_processor=candidate_processor,
        custom_functions=dict(),
        custom_filters=dict(),
    )
    parameters = candidate_processor(p)

output_animation=output_path/"output.mp4"
plot_animation(
    parameters=parameters,
    fps=60,
    output_path=output_animation,
)
print(f"Wrote {output_animation}")
