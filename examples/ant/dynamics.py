"""Dynamics for the Ant task."""
import torch
from typing import Optional
from environment import get_simulation
from environment import AntSimulation

def distance(p1: tuple[float, float], p2: tuple[float, float]) -> float:
    """Return the distance between the two points."""
    return ((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)**(1/2)

def simulation(parameters: torch.Tensor, env: Optional[AntSimulation]=None):
    """Run an ant simulation using the given parameters as control signal. The
    `parameters` define the time-varying control signal at each step and is
    expected to be of shape `(num_timesteps, 8)`."""
    # Initialize the environment
    if env is None:
        env = get_simulation(animation_fps=None)

    # Step simulation
    T = len(parameters)
    for t in range(T):
        ant_positions = env.step(parameters[t].detach().numpy())

        # Button-platform logic
        for ant_position in ant_positions:
            px, py, pr = env.button.x, env.button.y, env.button.r
            if distance(ant_position, (px, py)) < pr:  # ID: button_check
                env.activate_button()
