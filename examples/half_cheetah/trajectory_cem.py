"""CEM to solve half-cheetah."""
from examples.cem import cem
import gymnasium as gym
import torch
import math


def get_quality(x: torch.Tensor) -> float:
    env = gym.make("HalfCheetah-v4", reset_noise_scale=0.0)
    env.reset()
    final_x_position = -math.inf
    for t in range(len(x)):
        action = x[t].tolist()
        _, __, terminated, ____, info = env.step(action)
        final_x_position = info["x_position"]
        if terminated:
            break
    env.close()
    print("final_x_position", final_x_position)
    # Get to at least 4
    target_x_position = 8
    return final_x_position - target_x_position + 1


def get_cost(x: torch.Tensor) -> float:
    env = gym.make("HalfCheetah-v4", reset_noise_scale=0.0)
    env.reset()
    rewards = list()
    for t in range(len(x)):
        action = x[t].tolist()
        observation, reward, terminated, _, info = env.step(action)
        rewards.append(reward)
        if terminated:
            break
    env.close()
    return -sum(rewards)


Parameters = torch.Tensor
LogTime = float
LogEntry = tuple[LogTime, Parameters]
Log = list[LogEntry]
IsSolved = bool


def cem_solver(
        max_timesteps: int,
        timeout_s: float,
        ) -> tuple[Log, IsSolved]:
    # Maybe parametrize some of the magic constants
    log, is_solved = cem(
        max_timesteps=max_timesteps,
        action_size=6,
        sample_n=32,
        elite_n=4,
        horizon_len=20,
        init_stdev=0.3,
        cem_inner_iter_n=8,
        timeout_s=timeout_s,
        get_quality=get_quality,
        get_cost=get_cost,
        worker_n=1,
        verbose=True,
    )
    return log, is_solved
